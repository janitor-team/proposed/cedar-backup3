cedar-backup3 (3.2.0-4) unstable; urgency=low

  * Accept package cleanup from Debian Janitor.
    - Trim trailing whitespace
    - Add upstream metadata

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 16 Jul 2020 23:29:19 -0000

cedar-backup3 (3.2.0-3) unstable; urgency=medium

  * Bump standards version to 4.5.0.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 27 Jun 2020 17:57:56 +0000

cedar-backup3 (3.2.0-2) unstable; urgency=medium

  * Add __pycache__ directories to debian/clean.
  * Fix obsolete BitBucket URLs to point at GitHub.
    - Adjust debian/control and debian/copyright to set correct homepage
    - Adjust debian/watch to work properly with upstream release filenames

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 25 Aug 2019 14:20:15 +0000

cedar-backup3 (3.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Adjust debian/copyright to reflect new upstream copyright dates.
  * Move to debhelper compatibility level 12
    - Update debian/control to build depend on debhelper-compat (= 12)
    - Remove debian/compat, which is no longer needed
    - Add debian/clean so "debian/rules clean" works as expected
    - Adjust debian/rules to reflect new policy for documentation
    - Ajudst debian/cedar-backup3-doc.docs
    - Adjust debian/cedar-backup3-doc.doc-base.cedar-backup3-interface
    - Adjust debian/cedar-backup3-doc.doc-base.cedar-backup3-manual

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 24 Aug 2019 19:56:15 +0000

cedar-backup3 (3.1.12-6) unstable; urgency=medium

  * Bump standards version to 4.4.0.1 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 10 Aug 2019 19:43:47 +0000

cedar-backup3 (3.1.12-5) unstable; urgency=medium

  * Make testing improvements, including contributions from Ondřej Nový:
    - Use AUTOPKGTEST_ vars instead of ADT_ in test suite

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 20 Oct 2018 10:09:46 +0000

cedar-backup3 (3.1.12-4) unstable; urgency=medium

  * Bump standards version to 4.2.1.0 (no packaging changes).
  * Accept patch from Jelmer Vernooĳ to correct copyright file URI.
  * Fix Lintian warnings about debian/copyright file.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 16 Sep 2018 17:27:41 +0000

cedar-backup3 (3.1.12-3) unstable; urgency=medium

  * Bump standards version to 4.2.0.1 (no packaging changes).
  * Fix Lintian warning: remove obsolete X-Python3-Version from debian/control.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 07 Aug 2018 01:50:19 +0000

cedar-backup3 (3.1.12-2) unstable; urgency=medium

  * Convert Debian packaging from Subversion to Git.
  * Bump standards version to 4.1.3.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 03 Jan 2018 02:24:56 +0000

cedar-backup3 (3.1.12-1) unstable; urgency=medium

  * New upstream release, now with Sphinx API documentation instead of Epydoc.
    - Add libjs-jquery and libjs-underline to dependencies for cedar-backup3-doc
    - Adjust debian/rules to link in the pre-packaged Javascript libraries
  * Remove "Testsuite: autopkgtest" from debian/control per Lintian warning.
  * Bump standards version to 4.1.1.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 12 Nov 2017 19:24:27 +0000

cedar-backup3 (3.1.10-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 11 Sep 2017 01:43:58 +0000

cedar-backup3 (3.1.9-2) unstable; urgency=medium

  * Bump standards version to 4.0.1.0 (no packaging changes).
  * Add 'Recommends: fdisk | util-linux' in debian/control (closes: #872216).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 15 Aug 2017 23:47:09 +0000

cedar-backup3 (3.1.9-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright to list new copyright year 2017.
  * Update debian/control to recommend awscli (>= 1.11.13-1).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 03 Jul 2017 23:49:06 +0000

cedar-backup3 (3.1.8-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 4.0.0.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 02 Jul 2017 21:24:39 +0000

cedar-backup3 (3.1.7-2) unstable; urgency=medium

  * Suggest 'default-mysql-client|virtual-mysql-client' per new MySQL standard.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 05 Sep 2016 13:27:20 -0500

cedar-backup3 (3.1.7-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 3.9.8.0 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 21 Jun 2016 16:23:44 +0000

cedar-backup3 (3.1.6-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 13 Feb 2016 11:50:54 -0600

cedar-backup3 (3.1.5-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright to list new copyright year 2016.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 02 Jan 2016 21:22:09 +0000

cedar-backup3 (3.1.4-1) unstable; urgency=medium

  * New upstream release.
  * Fix FTBFS by changing build dependency to Python 3's python3-chardet.
  * Add support for the Debian continuous integration environment.
    - Update debian/control to list Testsuite: autopkgtest
    - Add debian/tests/control to list tests for CI environment
    - Change debian/rules to run unit tests via debian/smoketest
    - Start distributing CedarBackup3/testutil.py, which is required for tests

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 11 Aug 2015 18:29:39 +0000

cedar-backup3 (3.1.1-2) unstable; urgency=medium

  * Bump standards version to 3.9.6.1 (no packaging changes).
  * Fix all patches to eliminate the unnecessary leading # character.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 05 Aug 2015 13:52:41 -0500

cedar-backup3 (3.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 04 Aug 2015 14:21:02 -0500

cedar-backup3 (3.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Tue, 04 Aug 2015 01:30:29 +0000

cedar-backup3 (3.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 30 Jul 2015 22:39:50 -0500

cedar-backup3 (3.0.1-2) unstable; urgency=medium

  * Adjust debian/copyright to fix mistakes from copying cedar-backup2.
     * Make sure correct dates (though 2015) are listed for all items
     * Source code lines should reference CedarBackup3, not CedarBackup2

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 30 Jul 2015 21:57:20 -0500

cedar-backup3 (3.0.1-1) unstable; urgency=medium

  * Initial package release (closes: #794036).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 29 Jul 2015 21:08:21 -0500
