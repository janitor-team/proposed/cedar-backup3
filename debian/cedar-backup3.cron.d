# /etc/cron.d/cedar-backup3

#
# Below, comment out the appropriate set of scheduled tasks.  See the
# documentation in /usr/share/doc/cedar-backup3/manual for more information.
#

#Single machine (pool of one)
#30 00 * * * root  /usr/bin/cback3 all

# Client machine
#30 00 * * * root  /usr/bin/cback3 collect
#30 06 * * * root  /usr/bin/cback3 purge

# Master machine
#30 00 * * * root  /usr/bin/cback3 collect
#30 02 * * * root  /usr/bin/cback3 stage
#30 04 * * * root  /usr/bin/cback3 store
#30 06 * * * root  /usr/bin/cback3 purge

#
